package com.epam.exersices;

import com.epam.model.Property;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Array {
    private static Logger logger = LogManager.getLogger(Array.class);

    private Integer[] firstArray;
    private Integer[] secondArray;
    private Integer[] thirdArray;
    private Integer[] sameElement;
    private Integer[] differentElement;

    private static final int ALLOWABLE_DUBLICATES_COUNT;

    static {
        ALLOWABLE_DUBLICATES_COUNT = Integer.valueOf(
                Property.INSTANCE.getPropertyValue("ALLOWABLE_DUBLICATES_COUNT"));
    }

    public Array() {
        //Random filling:
        firstArray = new Integer[]{1, 2, 5, 6, 8, 5, 6};
        secondArray = new Integer[]{3, 4, 5, 6, 7, 5};
        thirdArray = new Integer[]{1, 2, 5, 3, 6, 5, 0, 0, 0, 5, 1, 1};
        sameElement = sameElements();
        differentElement = differentElements();
    }

    public void printResults() {
        printArray(firstArray, "First array:");
        printArray(secondArray, "Second array:");
        printArray(sameElement, "The same elements:");
        printArray(differentElement, "Not the same elements:");

        printArray(thirdArray, "Thirds array:");
        printArray(deleteMoreThanTwo(),
                "Thirds array, where are no 3 or more same elements: ");
        printArray(deleteMoreThanOneInline()
                , "Thirds array, where are no 2 or more same elements inline:");
    }

    public Integer[] sameElements() {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < firstArray.length; i++) {
            if (hasValue(secondArray, firstArray[i])) {
                if (!isContain(builder, "" + firstArray[i])) {
                    builder.append(firstArray[i] + "-");
                }
            }
        }
        String[] sameElement = builder.toString().split("-");
        Integer[] sameElements = new Integer[sameElement.length];
        for (int i = 0; i < sameElement.length; i++) {
            sameElements[i] = Integer.valueOf(sameElement[i]);
        }
        return sameElements;
    }

    public Integer[] differentElements() {
        Integer[] union = unionArray(firstArray, secondArray);
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < union.length; i++) {
            if (!hasValue(sameElement, union[i])) {
                builder.append(union[i] + "-");
            }
        }
        String[] differentElemString = builder.toString().split("-");
        Integer[] differentElements = new Integer[differentElemString.length];
        for (int i = 0; i < differentElements.length; i++) {
            differentElements[i] = Integer.valueOf(differentElemString[i]);
        }
        return differentElements;
    }

    public <T> boolean hasValue(T[] array, T value) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == value) {
                return true;
            }
        }
        return false;
    }

    public <T> void printArray(T[] array, String message) {
        logger.info(message);
        String print = "";
        for (T element : array) {
            print += element + " ";
        }
        logger.info(print);
    }

    public Integer[] unionArray(Integer[] first, Integer[] second) {
        Integer[] thirdArray = new Integer[first.length + second.length];
        for (int i = 0; i < thirdArray.length; i++) {
            if (i < first.length) {
                thirdArray[i] = first[i];
            } else {
                thirdArray[i] = second[i - first.length];
            }
        }
        return thirdArray;
    }

    public Integer[] deleteMoreThanTwo() {
        StringBuilder deleteIndexes = deleteIndexes(thirdArray);
        return deleteElements(deleteIndexes);
    }

    public Integer[] deleteMoreThanOneInline() {
        StringBuilder deleteIndexes = deleteIndexesInline(thirdArray);
        return deleteElements(deleteIndexes);
    }

    public Integer[] deleteElements(StringBuilder deleteIndexes) {
        int sizeArray = thirdArray.length - deleteIndexes.toString().split("-").length;
        Integer[] remainedElements = new Integer[sizeArray];
        for (int i = 0, j = 0; i < thirdArray.length; i++) {
            if (!isContain(deleteIndexes, "" + i)) {
                remainedElements[j] = thirdArray[i];
                j++;
            }
        }
        return remainedElements;
    }

    public boolean isContain(StringBuilder string, String value) {
        String[] indexes = string.toString().split("-");
        for (int i = 0; i < indexes.length; i++) {
            if (indexes[i].equals(value)) {
                return true;
            }
        }
        return false;
    }

    public StringBuilder deleteIndexes(Integer[] array) {
        StringBuilder deleteIndexes = new StringBuilder();
        for (int i = 0; i < array.length; i++) {
            int countDublicates = 1;
            StringBuilder temporaryIndexes = new StringBuilder();
            temporaryIndexes.append(i + "-");
            for (int j = i + 1; j < array.length; j++) {
                if (array[i] == array[j]) {
                    countDublicates++;
                    temporaryIndexes.append(j + "-");
                }
            }
            if (countDublicates > ALLOWABLE_DUBLICATES_COUNT) {
                appendIndexes(deleteIndexes, temporaryIndexes);
            }
        }
        return deleteIndexes;
    }

    public void appendIndexes(StringBuilder deleteIndexes, StringBuilder temporaryIndexes) {
        String[] indexesString = temporaryIndexes.toString().split("-");
        for (int j = 0; j < indexesString.length; j++) {
            if (!isContain(deleteIndexes, indexesString[j])) {
                deleteIndexes.append(indexesString[j] + "-");
            }
        }
    }

    public StringBuilder deleteIndexesInline(Integer[] array) {
        StringBuilder deleteIndexes = new StringBuilder();
        for (int i = 0; i < array.length; i++) {
            int countDublicates = 0;
            StringBuilder temporaryIndexes = new StringBuilder();
            loop:
            for (int j = i + 1; j < array.length; j++) {
                if (array[i] == array[j]) {
                    countDublicates++;
                    temporaryIndexes.append(j + "-");
                } else {
                    break;
                }
            }
            if (countDublicates > 0) {
                appendIndexes(deleteIndexes, temporaryIndexes);
            }
        }
        return deleteIndexes;
    }
}
