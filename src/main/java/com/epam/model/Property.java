package com.epam.model;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public enum Property implements AutoCloseable{
    INSTANCE;

    private static Logger logger = LogManager.getLogger(Property.class);

    private final Properties properties;

    Property(){
        properties = new Properties();
        close();
    }

    public String getPropertyValue(final String key){
        return properties.getProperty(key);
    }

    @Override
    public void close(){
        try(InputStream input = getClass().getClassLoader().getResourceAsStream("config.properties")) {
            properties.load(input);
        } catch (FileNotFoundException e) {
            logger.error("Property file not found!");
        } catch (IOException e) {
            logger.error("Problems with loading properties.");
        }
    }
}
