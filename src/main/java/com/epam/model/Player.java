package com.epam.model;

public class Player {
    private int strength;

    public Player() {
        strength = Integer.valueOf(
                Property.INSTANCE.getPropertyValue("strength"));
    }

    public void getStronger(final int strength) {
        this.strength += strength;
    }

    public void getWeaker(final int strength) {
        this.strength -= strength;
    }

    public int getStrength() {
        return strength;
    }
}
