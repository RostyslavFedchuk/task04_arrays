package com.epam.model;

import java.util.Random;

public class Door {
    private final int strength;
    private boolean isMonster;

    private static final int MIN_MOSNTER_STRENGTH;
    private static final int MAX_MOSNTER_STRENGTH;
    private static final int MIN_ARTIFACT_STRENGTH;
    private static final int MAX_ARTIFACT_STRENGTH;

    static {
        MIN_MOSNTER_STRENGTH = Integer.valueOf(
                Property.INSTANCE.getPropertyValue("MIN_MOSNTER_STRENGTH"));
        MAX_MOSNTER_STRENGTH = Integer.valueOf(
                Property.INSTANCE.getPropertyValue("MAX_MOSNTER_STRENGTH"));
        MIN_ARTIFACT_STRENGTH = Integer.valueOf(
                Property.INSTANCE.getPropertyValue("MIN_ARTIFACT_STRENGTH"));
        MAX_ARTIFACT_STRENGTH = Integer.valueOf(
                Property.INSTANCE.getPropertyValue("MAX_ARTIFACT_STRENGTH"));
    }

    public Door() {
        Random random = new Random();
        if (random.nextInt(2) == 1) {
            isMonster = true;
        }
        if (isMonster) {
            strength = MIN_MOSNTER_STRENGTH
                    + random.nextInt((MAX_MOSNTER_STRENGTH - MIN_MOSNTER_STRENGTH) + 1);
        } else {
            strength = MIN_ARTIFACT_STRENGTH
                    + random.nextInt((MAX_ARTIFACT_STRENGTH - MIN_ARTIFACT_STRENGTH) + 1);
        }
    }

    public int getStrength() {
        return strength;
    }

    public boolean isMonster() {
        return isMonster;
    }
}
