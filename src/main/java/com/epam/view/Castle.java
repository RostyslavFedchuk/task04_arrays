package com.epam.view;

import com.epam.controller.Room;
import com.epam.model.Player;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Castle {
    private static Logger logger = LogManager.getLogger(Castle.class);

    public static void main(String[] args) {
        play();
    }

    public static void play() {
        Room roundRoom = new Room(new Player());
        roundRoom.openAllDoors();
        logger.info("There is " + roundRoom.countDeathDoor(0)
                + " deathly rooms...");
        logger.info(roundRoom.goToWin());
    }
}
