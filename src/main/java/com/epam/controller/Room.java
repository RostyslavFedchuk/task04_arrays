package com.epam.controller;

import com.epam.model.Door;
import com.epam.model.Player;
import com.epam.model.Property;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Room {

    private static Logger logger = LogManager.getLogger(Room.class);

    private static final int COUNT_OF_DOORS;

    static {
        COUNT_OF_DOORS = Integer.valueOf(
                Property.INSTANCE.getPropertyValue("COUNT_OF_DOORS"));
    }

    private Door[] doors;
    private Player player;

    public Room(Player player) {
        doors = new Door[COUNT_OF_DOORS];
        for (int i = 0; i < COUNT_OF_DOORS; i++) {
            doors[i] = new Door();
        }
        this.player = player;
        logger.info("Welcome to the Magic Room.\n"
                + "There are " + COUNT_OF_DOORS + " doors:");
    }

    public void openAllDoors() {
        logger.info("    | Behind the door |   You achieve   |");
        for (int i = 0; i < doors.length; i++) {
            logger.info("_________________________________________");
            if (doors[i].isMonster()) {
                logger.info("| " + i + " | \tMonster\t\t  | -" + doors[i].getStrength() + " of strength |");
            } else {
                logger.info("| " + i + " | \tArtifact\t  | +" + doors[i].getStrength() + " of strength |");
            }
        }
    }

    public int countDeathDoor(int indexDoor) {
        int countDeathDoor = 0;
        if (indexDoor < 0 || indexDoor >= COUNT_OF_DOORS) {
            return 0;
        }
        if (doors[indexDoor].isMonster()) {
            player.getWeaker(doors[indexDoor].getStrength());
        }
        if (player.getStrength() < 0) {
            countDeathDoor++;
            player.getStronger(doors[indexDoor].getStrength());
        }
        return countDeathDoor + countDeathDoor(indexDoor + 1);
    }

    public String goToWin() {
        StringBuilder artefactsIndexes = new StringBuilder("But there is a good solution for you!\n"
                + "The winning door opening combination is: ");
        StringBuilder monsterIndexes = new StringBuilder();
        int monstersDamage = 0;
        for (int i = 0; i < COUNT_OF_DOORS; i++) {
            if (!doors[i].isMonster()) {
                artefactsIndexes.append(i + "->");
                player.getStronger(doors[i].getStrength());
            } else {
                monstersDamage += doors[i].getStrength();
                monsterIndexes.append(i + "->");
            }
        }
        if (player.getStrength() >= monstersDamage) {
            artefactsIndexes.append(monsterIndexes + "WIN");
        } else {
            artefactsIndexes = new StringBuilder("There is no chance for you to win! Go home, pal...");
        }
        return artefactsIndexes.toString();
    }
}
